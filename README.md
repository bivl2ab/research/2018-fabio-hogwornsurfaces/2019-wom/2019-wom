This code is part of the published paper: "Gonzalez-Arias, C., Viáfara, C. C., Coronado, J. J., & Martinez, F. (2019). Automatic classification of severe and mild wear in worn surface images using histograms of oriented gradients as descriptor. Wear, 426, 1702-1711."

Please cite our paper if you use the code.

---
`` @article{gonzalez2019automatic,
  title={Automatic classification of severe and mild wear in worn surface images using histograms of oriented gradients as descriptor},
  author={Gonzalez-Arias, C and Vi{\'a}fara, CC and Coronado, JJ and Martinez, F},
  journal={Wear},
  volume={426},
  pages={1702--1711},
  year={2019},
  publisher={Elsevier}
 } ``
---


## Wear_Classification
 **Fabio Martínez, Camilo Gonzalez, Cristian Viafara**
### Universidad Industrial de Santander (UIS). Colombia




